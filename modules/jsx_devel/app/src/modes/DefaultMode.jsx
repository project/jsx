import jsxIcon from '../../icons/jsx.svg';
import twigIcon from '../../icons/twig.svg';
export const DefaultMode = () => {
  return (
    <>
      <p className="mb-4">
        <strong>JSX mode</strong> shows what is rendered by JSX.{' '}
        <u>Click any <img className="w-8 inline" src={jsxIcon}/> button</u> to view the component&apos;s file name
        and available props from Drupal in your browser console.
      </p>
      <p>
        <strong>Twig mode</strong> shows what is rendered by Twig.{' '}
        <u>Click any <img className="w-8 inline" src={twigIcon}/> button</u> to view the corresponding twig
        template.
      </p>
    </>
  );
};
