export const JsxMode = (renderedBy, variants) => {
  return (
    <>
      <p className="mb-4">
        JSX component is rendered by: {''}
        <strong>
          <u>{renderedBy}</u>
        </strong>
      </p>
      <p className="mb-4">
        <strong>📣️ Open the browser console</strong> to view available props
        you can use in your component.
      </p>
      <p>
        The following more specific{' '}
        <strong>
          <i>variants</i>
        </strong>{' '}
        of this component can be created:
        {variants[renderedBy].map((v, index) => {
          return <li key={index}>{v}</li>;
        })}
      </p>
    </>
  );
};
