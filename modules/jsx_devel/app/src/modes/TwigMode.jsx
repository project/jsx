export const TwigMode = (renderedBy) => {
  return (
    <>
      <p className="mb-4">
        Twig element is rendered by: {''}
        <strong>
          <u>{renderedBy}</u>
        </strong>
      </p>
    </>
  );
};
