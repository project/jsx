export function isVisible(element) {
  if (element.classList.contains('visually-hidden') || element.parentElement.classList.contains('visually-hidden')) {
    return false;
  } else {
    // https://github.com/jquery/jquery/blob/main/src/css/hiddenVisibleSelectors.js#L9
    return !!(element.offsetWidth || element.offsetHeight || element.getClientRects().length);
  }
}
