import React from 'react';
import { createRoot } from 'react-dom/client';
import DebugBar from './DebugBar';
import '../input.css';
import {isVisible} from './utils';

const container = document.createElement('div');
container.setAttribute('id', 'debug-tool-container');
document.querySelector('template[hyperscriptify]').nextElementSibling.after(container);

// Create a portal element for each JSX component.
document.querySelectorAll('[data-drupal-prop-info]').forEach((element) => {
  const uuid = element.getAttribute('data-drupal-prop-info');
  const portal = document.createElement('div');
  portal.setAttribute('id', `portal-root-${uuid}`);
  portal.setAttribute('class', 'portal-root');
  element.prepend(portal);
})

// Create a portal element for each Twig element.
document.querySelectorAll('[data-drupal-twig-template]').forEach((element) => {
  if (isVisible(element)) {
    const uuid = element.getAttribute('data-twig-uuid');
    const portal = document.createElement('div');
    portal.setAttribute('id', `portal-root-${uuid}`);
    portal.setAttribute('class', 'portal-root');
    element.prepend(portal);
  }
})

const root = createRoot(container);
root.render(
  <DebugBar/>
);
