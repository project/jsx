import {useState, useEffect, createContext} from 'react';
import {JsxMode} from './modes/JsxMode';
import {TwigMode} from './modes/TwigMode';
import {DefaultMode} from './modes/DefaultMode';
import DebugButton from './DebugButton';
import {isVisible} from './utils';
import ChevronUpIcon from '../icons/chevron-up.svg';
import ChevronDownIcon from '../icons/chevron-down.svg';

export const ButtonRectsListContext = createContext(null);

export default function DebugBar() {
  const [debugMode, setDebugMode] = useState('off');
  const [targetComponentsData, setTargetComponentsData] = useState([]);
  const [renderedBy, setRenderedBy] = useState('');
  const [variants, setVariants] = useState({});
  // Initialize state for rect values of every debug button.
  const [rectsList, setRectsList] = useState([]);
  const [isExpanded, setIsExpanded] = useState(true);

  useEffect(() => {
    const componentList = [];
    if (debugMode === 'jsx') {
      const jsxElements = document.querySelectorAll('[data-drupal-prop-info]');
      const variantsObj = {};
      jsxElements.forEach((element) => {
        const allVariants = element.hasAttribute('data-drupal-jsx-suggestions')
          ? element.getAttribute('data-drupal-jsx-suggestions').split(',')
          : [];
        const template = element.getAttribute('data-drupal-jsx-template');
        // Get variant suggestions.
        const variantSuggestions = [];
        allVariants.forEach((variant) => {
          if (variant.charAt(0) !== 'x') {
            variantSuggestions.push(variant.substring(2));
          }
        });
        variantsObj[template] = variantSuggestions;
        const obj = {
          template,
          uuid: element.getAttribute('data-drupal-prop-info'),
        };
        componentList.push(obj);
      });
      setVariants(variantsObj);
    } else if (debugMode === 'twig') {
      const targetElements = document.querySelectorAll(
        '[data-drupal-twig-template]',
      );
      targetElements.forEach((element) => {
        if (isVisible(element)) {
          const obj = {
            template: element.getAttribute('data-drupal-twig-template'),
            uuid: element.getAttribute('data-twig-uuid'),
          };
          componentList.push(obj);
        }
      });
    }
    setTargetComponentsData(componentList);
    document
    .querySelector('template[hyperscriptify]').nextElementSibling
    .setAttribute('data-jsx-debug', debugMode);
  }, [debugMode]);

  function onClickHandler(e) {
    if (document.querySelectorAll('.keep-highlight')) {
      document.querySelectorAll('.keep-highlight').forEach((el) => {
        el.classList.remove('keep-highlight');
      })
    }
    setRenderedBy('');
    setDebugMode(e.target.value);
  }

  function collapseBar() {
    setIsExpanded(!isExpanded);
  }

  const showMessage = () => {
    switch (debugMode) {
      case 'jsx':
        return JsxMode(renderedBy, variants);
      case 'twig':
        return TwigMode(renderedBy);
      default:
        return DefaultMode();
    }
  };

  const buttonClasses = "px-4 py-2 text-sm font-medium text-gray-900 bg-white border-gray-200 hover:bg-gray-100 hover:text-blue-700 focus:z-10 focus:ring-2 focus:ring-blue-700 focus:text-blue-700 dark:bg-gray-700 dark:border-gray-600 dark:text-white dark:hover:text-white dark:hover:bg-gray-600 dark:focus:ring-blue-500 dark:focus:text-white"

  return (
      <>
        <div className="container mr-auto ml-1 px-4 pb-4 h-full"
             style={{
               height: isExpanded ? 'auto' : '70px'
             }}
        >
          <div className="inline-flex justify-between items-center h-full">
            <div className="inline-flex rounded-md shadow-sm mr-3" role="group">
              <button
                onClick={(e) => onClickHandler(e)}
                type="button"
                value="jsx"
                className={`border rounded-s-lg ${buttonClasses}`}
                aria-label="jsx mode"
                style={{
                  backgroundColor: debugMode === 'jsx' ? '#e5e5e5' : null
                }}
              >
                JSX
              </button>
              <button
                onClick={(e) => onClickHandler(e)}
                type="button"
                value="twig"
                className={`border-t border-b ${buttonClasses}`}
                aria-label="twig mode"
                style={{
                  backgroundColor: debugMode === 'twig' ? '#e5e5e5' : null
                }}
              >
                Twig
              </button>
              <button
                onClick={(e) => onClickHandler(e)}
                type="button"
                value="off"
                className={`border rounded-e-lg ${buttonClasses}`}
                aria-label="debug off"
                style={{
                  backgroundColor: debugMode === 'off' ? '#e5e5e5' : null
                }}
              >
                OFF
              </button>
            </div>
            <div className="mt-3"
                 style={{display: isExpanded ? 'block' : 'none'}}
            >
              {renderedBy ? showMessage() : <DefaultMode/>}
            </div>
          </div>
          <div className="absolute top-2 right-2">
            <button onClick={collapseBar} aria-label="collapse button" className="circle w-9 h-9 rounded-full flex items-center justify-center bg-white hover:bg-[#e5e5e5]">
              <img className="h-8" src={isExpanded ? ChevronDownIcon : ChevronUpIcon}/>
            </button>
          </div>
        </div>
        <ButtonRectsListContext.Provider value={{rectsList, setRectsList}}>
          {targetComponentsData.length > 0
            ? targetComponentsData.map((item, i) => {
              if (item.uuid && item.uuid.length) {
                return (
                    <DebugButton
                      key={i}
                      componentData={item}
                      debugMode={debugMode}
                      setRenderedBy={setRenderedBy}
                      setIsExpanded={setIsExpanded}
                    />
                );
              }
            })
          : null}
        </ButtonRectsListContext.Provider>
      </>
  );
}
