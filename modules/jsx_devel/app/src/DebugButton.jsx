import {
  useState,
  useEffect,
  useRef,
  useContext,
} from 'react';
import { createPortal } from 'react-dom';
import jsxIcon from '../icons/jsx.svg';
import twigIcon from '../icons/twig.svg';
import { ButtonRectsListContext } from './DebugBar';

export default function DebugButton({componentData, debugMode, setRenderedBy, setIsExpanded}) {
  const [domReady, setDomReady] = useState(false);
  const { template, uuid } = componentData;
  const buttonRef = useRef();
  const {rectsList, setRectsList} = useContext(ButtonRectsListContext);
  const [overlapStyles, setOverlapStyles] = useState({});

  function onMouseEnter() {
    buttonRef.current.parentElement.parentElement.classList.add('debug-highlight');
  }

  function onMouseLeave() {
    buttonRef.current.parentElement.parentElement.classList.remove('debug-highlight');
  }

  useEffect(() => {
    setDomReady(true);
  }, []);

  // Check for overlapping buttons and update left or top positions in context
  // if necessary.
  useEffect(() => {
    if (domReady && buttonRef.current) {
      let currentRect = buttonRef.current.getBoundingClientRect();
      let newLeft;
      let newTop;
      setRectsList(prevRect => {
        if (prevRect.length > 0) {
          const [priorRect] = prevRect.slice(-1);
          const [priorKey] = Object.keys(priorRect);
          const prior = priorRect[priorKey];
          const overlap = !(currentRect.right < prior.left ||
            currentRect.left > prior.right ||
            currentRect.bottom < prior.top ||
            currentRect.top > prior.bottom)
          if (overlap) {
            let newRect;
            if (currentRect.left < prior.right) {
              newLeft = prior.right - currentRect.left + 6;
              newRect = {...currentRect, offsetLeft: newLeft};
            } else if (currentRect.top < prior.bottom) {
              newTop = prior.bottom - currentRect.top + 6;
              newRect = {...currentRect, offsetTop: newTop};
            }
            if (newRect) {
              return [...prevRect, {[uuid]: newRect}]
            }
          }
        }
        return [...prevRect, {[uuid]: currentRect}]
      });
    }
  }, [domReady]);

  // Set overlap styles based on context's rect list values.
  useEffect(() => {
    if (rectsList.length) {
      const currentRect = rectsList.find((rect) => rect[uuid]);
      if (currentRect && currentRect[uuid] && currentRect[uuid].offsetLeft) {
        setOverlapStyles({'left': currentRect[uuid].offsetLeft});
      }
      if (currentRect && currentRect[uuid] && currentRect[uuid].offsetTop) {
        setOverlapStyles({'top': currentRect[uuid].offsetTop});
      }
    }
  }, [rectsList]);

  if (!debugMode) {
    return null;
  }

  function onClickHandler(e) {
    // Block default click handling. For example if the debug button is on top of an <input type="submit"/>.
    e.preventDefault();
    if (document.querySelectorAll('.keep-highlight')) {
      document.querySelectorAll('.keep-highlight').forEach((el) => {
        el.classList.remove('keep-highlight');
      })
    }
    buttonRef.current.parentElement.parentElement.classList.add('keep-highlight');
    setRenderedBy(template);
    setIsExpanded(true);
    if (debugMode === 'jsx') {
      console.groupCollapsed(`View available props for: ${template}`);
      console.log(
        `Props that can be added to the PropTypes definition in %c${template}`,
        'font-style: italic',
        `(click arrow to expand)`,
      );
      console.log(drupalSettings.jsx[uuid]);
      console.groupEnd();
    }
  }

  const ButtonElement = () => {
    return (
      <button
        ref={buttonRef} style={overlapStyles}
        onMouseEnter={onMouseEnter}
        onMouseLeave={onMouseLeave}
        onClick={(e) => onClickHandler(e)}
        className="debug-btn"
      >
        <img className="debug-btn-img" src={debugMode === 'jsx' ? jsxIcon : twigIcon} />
      </button>
    );
  };

  return domReady && document.getElementById(`portal-root-${uuid}`)
    ? createPortal(<ButtonElement />, document.getElementById(`portal-root-${uuid}`))
    : null;
}
