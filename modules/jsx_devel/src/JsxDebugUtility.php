<?php

namespace Drupal\jsx_devel;

use Drupal\Component\Render\MarkupInterface;
use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Template\Attribute;
use Symfony\Component\DependencyInjection\Container as Con;
use Symfony\Component\String\UnicodeString;

class JsxDebugUtility {

  protected static array $themeRegistryOnlyModules = [];

  /**
   * Adds twig template information to a data attribute in the variables array.
   * @param array $variables
   *   The variables array.
   * @param string $template_file
   *   The template being rendered.
   */
  static public function addTwigDebugInfo(array &$variables, string $template_file): void {
    if (str_starts_with($template_file, 'core/')) {
      $variables['attributes']['data-drupal-twig-template'] = basename($template_file);
      $branch_or_tag = \Drupal::VERSION;
      if (str_ends_with($branch_or_tag, '-dev')) {
        $branch_or_tag = substr($branch_or_tag, 0, strrpos($branch_or_tag, '.')) . '.x';
      }
      $variables['attributes']['data-drupal-twig-template-href'] = "https://git.drupalcode.org/project/drupal/-/blob/{$branch_or_tag}/{$template_file}";
    }
    else {
      $variables['attributes']['data-drupal-twig-template'] = $template_file;
    }
    $uuid = \Drupal::service('uuid')->generate();
    $variables['attributes']['data-twig-uuid'] = $uuid;
    // @todo The following doesn't yet work correctly for module-provided Twig
    //   templates because it relies on variables that aren't yet set during
    //   hook_preprocess().
    /*
    $suggestions = self::themeHookSuggestions($variables, $template_file);
    if (!empty($suggestions)) {
      $variables['attributes']['data-drupal-twig-template-suggestions'] = implode(',', $suggestions);
    }
    */
  }

  /**
   * Largely borrowed from twig_render_template().
   *
   * @param $variables
   * @param $template_file
   * @param $extension
   *
   * @return array
   */
  static public function themeHookSuggestions($variables, $template_file, $extension = '.html.twig') {
    // If there are theme suggestions, reverse the array so more specific
    // suggestions are shown first.
    $suggestions = [];
    if (!empty($variables['theme_hook_suggestions'])) {
      $variables['theme_hook_suggestions'] = array_reverse($variables['theme_hook_suggestions']);
    } else {
      $variables['theme_hook_suggestions'] = [];
    }
    // Add debug output for directly called suggestions like
    // '#theme' => 'comment__node__article'.
    if (str_contains($variables['theme_hook_original'], '__')) {
      $derived_suggestions[] = $hook = $variables['theme_hook_original'];
      while ($pos = strrpos($hook, '__')) {
        $hook = substr($hook, 0, $pos);
        $derived_suggestions[] = $hook;
      }
      // Get the value of the base hook (last derived suggestion) and append it
      // to the end of all theme suggestions.
      $base_hook = array_pop($derived_suggestions);
      $variables['theme_hook_suggestions'] = array_merge($derived_suggestions, $variables['theme_hook_suggestions']);
      $variables['theme_hook_suggestions'][] = $base_hook;
    }
    if (!empty($variables['theme_hook_suggestions'])) {
      $current_template = basename($template_file);
      $suggestions = $variables['theme_hook_suggestions'];
      // Only add the original theme hook if it wasn't a directly called
      // suggestion.
      if (!str_contains($variables['theme_hook_original'], '__')) {
        $suggestions[] = $variables['theme_hook_original'];
      }
      $base_hook = $base_hook ?? $variables['theme_hook_original'];
      foreach ($suggestions as $key => &$suggestion) {
        // Valid suggestions are $base_hook, $base_hook__*, and contain no hyphens.
        if (($suggestion !== $base_hook && !str_starts_with($suggestion, $base_hook . '__')) || str_contains($suggestion, '-')) {
          unset($suggestions[$key]);
          continue;
        }
        $template = strtr($suggestion, '_', '-') . $extension;
        $prefix = ($template == $current_template) ? 'x' : '*';
        $suggestion = $prefix . ' ' . $template;
      }
    }

    return $suggestions;
  }

  /**
   * Associates custom elements rendered by the JSX engine to debug data.
   *
   * @param array|object $value
   *   The custom element render array.
   * @param $tagName
   *   The custom element tag name of the template being rendered.
   * @param $uuid
   *    A unique ID for associating this element with a debug data available
   *    in JavaScript.
   */
  static public function addJsxDebugInfo(array|object &$value, string $tagName, string $uuid, $variables): void {
    $value = (array) $value;
    $value['data-drupal-prop-info'] = $uuid;
    $component_name = self::jsxComponentNameFromTagName($tagName);
    $component_root = explode('--', $component_name)[0];

    $value['data-drupal-jsx-template'] = self::jsxComponentNameFromTagName($tagName) . '.jsx';
    $hook = $variables["theme_hook_original"] ?? '';
    $suggestions = self::themeHookSuggestions($variables, $value['data-drupal-jsx-template'], '.jsx');
    if (!empty($suggestions)) {
      $jsx_suggestions = array_map(function($item) use ($component_root, $hook, $component_name) {
        $base_hook = explode('__', $hook, 2)[0];
        $base_hooked = implode($component_root, explode($base_hook, $item, 2));
        $component_named =  self::jsxComponentNameFromTagName(pathinfo($base_hooked)['filename']);
        $prefix = ($component_name === $component_named) ? 'x' : '*';
        return "$prefix $component_named.jsx";
      }, $suggestions);
      $value['data-drupal-jsx-suggestions'] = implode(',', array_reverse($jsx_suggestions));
    }
  }

  static private function jsxComponentNameFromTagName($tagName) {
    return implode('--', array_map(function($part) {
      return ucfirst((string) (new UnicodeString($part))->camel());
    }, explode('--', $tagName)));
  }

  static private function getTwigSourceCode($jsx_file_name) {
    if (empty(static::$themeRegistryOnlyModules) && $cache = \Drupal::cache()->get('theme_registry:build:modules')) {
      static::$themeRegistryOnlyModules = $cache->data;
    }
    $hook = strtr(basename($jsx_file_name, '.template-info.json'), '-', '_');
    $info = NULL;
    while ($hook) {
      $info = static::$themeRegistryOnlyModules[$hook] ?? NULL;
      if (isset($info)) {
        break;
      }
      if ($pos = strrpos($hook, '__')) {
        $hook = substr($hook, 0, $pos);
      }
      else {
        break;
      }
    }
    if ($info) {
      $twig_template = $info['path'] . '/' . $info['template'] . '.html.twig';
      return file_get_contents($twig_template);
    }
  }

  /**
   * The initial step to generating prop data for custom element debugging.
   *
   * @param Attribute $available_props
   *   An array of props available to JSX
   * @param array $element
   *   The element to be rendered
   * @param array $variables
   *   The original render array.
   * @param $filename
   *   The file name of the template being rendered.
   * @param $uuid
   *    A unique ID for associating this element with a debug data available
   *    in JavaScript.

   */
  static public function addJsxPropInfo(Attribute &$available_props, array &$element, array $variables, string $filename, string $uuid): void {
    $available_props = self::getAvailableProps($variables, static::getTwigSourceCode($filename));
    //$available_props['findonpage'] = [$uuid];
    //$available_props['filename'] = $filename;
    $element['#attached']['drupalSettings']['jsx'][$uuid] = $available_props;
    $element['#attached']['library'][] = 'jsx_devel/jsx_devel.debug';
  }

  /**
   * Determines what props are available to the JSX code, and provides previews
   * of the content values where possible
   *
   * @param array $variables
   *   The variables array.
   *
   * @return array|array[]|mixed|string|string[]
   *   A variation of the variables array, that provides information regarding
   *   the data types and values of its contents.
   */
  static public function getAvailableProps(array $variables, $twig_source_code) {
    $var_children = array_filter($variables, fn($key) => $key[0] !== '#', ARRAY_FILTER_USE_KEY);
    self::addTypeToKeys($var_children, $twig_source_code);
    $prop_preview = self::getPropPreview($var_children, $twig_source_code);

    return $prop_preview;
  }

  /**
   * Recursively travels a render array and returns information about its props.
   *
   * @param mixed $content
   *   The item within the render array being checked
   *
   * @return array|array[]|mixed|string|string[]
   */
  static public function getPropPreview($content, $twig_source_code, $recursion_level = 0) {
    if ($recursion_level > 10) {
      return 'Information not available at this level of nesting';
    }
    if (is_object($content)) {
      $class = get_class($content);
      if ($content instanceof Attribute) {
        return $content->toArray();
      }
      elseif ($content instanceof MarkupInterface && !empty((string) $content)) {
        return (string) $content;
      }
      elseif (!$content instanceof ContentEntityInterface) {
        return $class;
      }
      return self::getUnpackedContentEntity($content);
    }

    if (!is_array($content)) {
      return $content;
    }

    foreach ($content as $key => $value) {
      if ($key !== '~...') {
        self::addTypeToKeys($value, $twig_source_code);
        $content[$key] = self::getPropPreview($value, $twig_source_code, $recursion_level + 1);
      }
      else {
        foreach ($value as $key2 => $value2) {
          // For props already hidden behind a "...", we don't need to hide
          // their sub-properties behind another one.
          $twig_source_code = '';
          self::addTypeToKeys($value2, $twig_source_code);
          $content[$key][$key2] = self::getPropPreview($value2, $twig_source_code, $recursion_level);
        }
      }
    }
    return $content;
    /*
    return array_map(function($item) {
      self::addTypeToKeys($item, $twig_source_code);
      return self::getPropPreview($item, $twig_source_code);
    }, $content);
    */
  }

  /**
   * Checks the top level values of an array and updates the key names to indicate
   * what template types would work with it based on that value.
   *
   * @param mixed $item
   *   The data to check.
   */
  static public function addTypeToKeys(&$item, $twig_source_code): void {
    if (is_array($item)) {
      $final = [];
      foreach($item as $key => &$value) {
        $hide = FALSE;
        if (substr($key, 0, 1) !== '#') {
          $type = gettype($value);
          if ($type === 'array') {
            if (empty($value)) {
              $type = '?';
            }
            elseif (self::canArrayRender($value)) {
              $type = 'PropTypes.element';
            }
            elseif (array_is_list($value)) {
              $type = 'PropTypes.arrayOf';
            }
            else {
              $type = 'PropTypes.shape';
            }
          }
          elseif ($type === 'object') {
            if ($value instanceof Attribute) {
              $type = 'PropTypes.object';
            }
            elseif ($value instanceof MarkupInterface) {
              $type = 'PropTypes.element';
            }
            else {
              $type = 'PropTypes.shape';
            }
          }
          elseif ($type === 'string') {
            $type = 'PropTypes.string';
          }
          elseif ($type === 'boolean') {
            $type = 'PropTypes.boolean';
          }
          elseif ($type === 'integer' || $type === 'double') {
            $type = 'PropTypes.number';
          }

          if (!is_numeric($key) && !str_contains($twig_source_code, $key)) {
            $hide = TRUE;
          }
          $key_camel_cased = (string) (new UnicodeString($key))->camel();
          $key = "$key_camel_cased ($type)";
        }
        if ($hide) {
          // Add a '~' in front of the '...' so that Chrome console orders it
          // last (the console sorts keys, '.' is before letters, and '~' is
          // after).
          $final['~...'][$key] = $value;
        }
        else {
          $final[$key] = $value;
        }
      }
      $item = $final;
    }
    elseif (is_string($item)) {
      $item = trim($item);
    }
  }

  /**
   * Traverses a content entity and returns a preview of the available values.
   *
   * @param mixed $content
   *   The content being checked.
   * @param int $depth
   *   The level of recursion. Recursion stops after 4.
   *
   * @return array
   *   A preview of the content entity values.
   */
  static public function getUnpackedContentEntity($content, int $depth = 0): array {
    $fields = $content->getFields();
    $list = [];
    foreach (array_keys($fields) as $field_name) {
      $return = NULL;
      $data_type = NULL;
      try {
        $field_list = $content->get($field_name);
        if ($field_list instanceof EntityReferenceFieldItemListInterface) {
          $referenced_entities = $field_list->referencedEntities();
          $entity_summaries = array_map(function($referenced_entity) use (&$data_type, $depth) {
            $data_type = get_class($referenced_entity);
            $entity_id = $referenced_entity->id();
            $entity_label = $referenced_entity->label();
            $is_feildy = $referenced_entity instanceof ContentEntityInterface;
            if ($is_feildy && $depth <= 4) {
              $unpacked = self::getUnpackedContentEntity($referenced_entity, $depth + 1);
              return $unpacked;
            }
            return "$entity_label <$entity_id>";
          }, $referenced_entities);
          $return = count($entity_summaries) === 1 ? $entity_summaries[0] : !empty($entity_summaries);
        } else {
          $type = $field_list->getDataDefinition()->getType();
          $dd = $field_list->getDataDefinition();
          $property_definition = $dd->getPropertyDefinition('value');
          $data_type = $property_definition->getDataType();
          $values = array_map(fn($item) => $item['value'], $field_list->getValue());
          $return = count($values) === 1 ? $values[0] : $values;
        }
      } catch(\Throwable $e) {
        continue;
      }

      if (!is_null($return)) {
        $js_field_name = lcfirst(Con::camelize($field_name));
        $js_data_type = $data_type ?? 'string';
        if (is_object($return)) {
          $list[$js_field_name] = get_class($return);
        } elseif(is_array($return)) {
          if (empty($return)) {
            $list[$js_field_name] = 'NULL';
          } else {
            $list["$js_field_name ({$js_data_type}[])"] = $return;
          }
        }
        else {
          $type = $js_data_type ?? gettype($return);
          $list["$js_field_name ($type)"] = $return;
        }
      }
    }

    return $list;
  }

  /**
   * Checks if an array has renderable children or declares a theme or type.
   *
   * @param $array
   *   The potentially renderable array.
   *
   * @return boolean
   *   True if the array is renderable.
   */
  static public function canArrayRender($array): bool {
    if (!is_array($array)) {
      return FALSE;
    }

    if (array_key_exists('#theme', $array) || array_key_exists('#type', $array)) {
      return TRUE;
    }

    foreach($array as $value) {
      if (self::canArrayRender($value)) {
        return TRUE;
      }
    }

    return FALSE;
  }

}
