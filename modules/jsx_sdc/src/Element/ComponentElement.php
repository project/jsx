<?php

namespace Drupal\jsx_sdc\Element;

use Drupal\sdc\Element\ComponentElement as SdcComponentElement;

/**
 * Extends the SDC module's "component" render element type.
 *
 * @RenderElement("component")
 */
class ComponentElement extends SdcComponentElement {

  /**
   * @inheritDoc
   */
  public function preRenderComponent(array $element): array {
    /** @var \Drupal\sdc\ComponentPluginManager $component_manager */
    $component_manager = \Drupal::service('plugin.manager.sdc');
    $component_definition = $component_manager->getDefinition($element['#component']);

    /** @var \Drupal\Core\Extension\ThemeExtensionList $theme_list */
    $theme_list = \Drupal::service('extension.list.theme');
    if ($theme_list->exists($component_definition['provider'])) {
      $theme = $theme_list->get($component_definition['provider']);
      if ($theme->engine === 'jsx') {
        return jsx_pre_render_component($element);
      }
    }

    // If we didn't already return before here, fall back to SDC's Twig-based
    // rendering.
    return parent::preRenderComponent($element);
  }

}
