/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    'app/**/*.{js,ts,jsx,tsx,mdx}', // Note the addition of the `app` directory.
  ],
  theme: {
    extend: {
      screens: {
        md: '48em',
      },
      fontFamily: {
        scopeOne: ['"Scope One"', 'Georgia', 'serif'],
      },
    },
  },
  plugins: [],
};
