import clsx from 'clsx';
import a2p from '@/lib/a2p';
import cleanClass from '@/lib/utils';

export default function Field({
  fieldName,
  fieldType,
  label,
  labelDisplay,
  labelHidden,
  attributes,
  items,
  titleAttributes,
  multiple,
}) {
  // umami_jsx_preprocess_field
  function getFieldClasses() {
    const classes = [];
    if (fieldName) {
      if (
        ['field_recipe_category', 'field_tags', 'field_difficulty'].includes(
          fieldName,
        )
      ) {
        classes.push('label-items');
      }
    }
    if (fieldType.includes('text')) {
      classes.push('clearfix', 'text-formatted');
    }
    return classes;
  }

  const classes = [
    'field',
    `field--name-${cleanClass(fieldName)}`,
    `field--type-${cleanClass(fieldType)}`,
    `field--label-${cleanClass(labelDisplay)}`,
    labelDisplay === 'inline' && 'clearfix',
    getFieldClasses(),
  ];
  const titleClasses = [
    'field__label',
    labelDisplay === 'visually_hidden' && 'visually-hidden',
  ];

  function renderItems() {
    return items.map((item, index) => (
      <div key={index} {...a2p(item.attributes, { class: 'field__item' })}>
        {item.content}
      </div>
    ));
  }

  if (labelHidden) {
    if (multiple) {
      return (
        <div {...a2p(attributes, { class: clsx(classes, 'field__items') })}>
          {renderItems()}
        </div>
      );
    }
    return items.map((item, index) => (
      <div
        key={index}
        {...a2p(attributes, { class: clsx(classes, 'field__item') })}
      >
        {item.content}
      </div>
    ));
  }
  return (
    <div {...a2p(attributes, { class: classes })}>
      <div {...a2p(titleAttributes, { class: clsx(titleClasses) })}>
        {label}
      </div>
      {multiple ? (
        <div className="field__items">{renderItems()}</div>
      ) : (
        renderItems()
      )}
    </div>
  );
}
