export default function Page({ page, isFront, node }) {
  return (
    <div className="layout-container">
      {(page.preHeader || page.header) && (
        <PageHeader
          preHeader={page.preHeader}
          header={page.header}
        ></PageHeader>
      )}

      {page.highlighted && (
        <PageHighlighted highlighted={page.highlighted}></PageHighlighted>
      )}

      {page.tabs && (
        <div className="layout-tabs">
          <div className="container">{page.tabs}</div>
        </div>
      )}

      {page.bannerTop && (
        <div className="layout-banner-top">{page.bannerTop}</div>
      )}

      {page.breadcrumbs && (
        <div className="layout-breadcrumbs">
          <div className="container">{page.breadcrumbs}</div>
        </div>
      )}

      {!node && page.pageTitle && (
        <div className="layout-page-title">
          <div className={`${isFront ? 'is-front ' : ''}container`}>
            {page.pageTitle}
          </div>
        </div>
      )}

      <main role="main" className="main container">
        <div className="layout-content">
          <a id="main-content" tabIndex="-1"></a>
          {page.content}
        </div>
        {page.sidebar && (
          <aside className="layout-sidebar" role="complementary">
            {page.sidebar}
          </aside>
        )}
      </main>

      {page.contentBottom && (
        <PageContentBottom contentBottom={page.contentBottom} />
      )}

      {page.footer && <PageFooter footer={page.footer} />}

      {page.bottom && <PageBottom bottom={page.bottom} />}
    </div>
  );
}

export function PageHeader({ preHeader, header }) {
  return (
    <header className="layout-header" role="banner">
      <div className="container">
        {preHeader}
        {header}
      </div>
    </header>
  );
}

export function PageHighlighted({ highlighted }) {
  return (
    <div className="layout-highlighted">
      <div className="container">{highlighted}</div>
    </div>
  );
}

export function PageContentBottom({ contentBottom }) {
  return <div className="layout-content-bottom">{contentBottom}</div>;
}

export function PageFooter({ footer }) {
  return (
    <div className="layout-footer">
      <footer className="footer" role="contentinfo">
        <div className="container">{footer}</div>
      </footer>
    </div>
  );
}
export function PageBottom({ bottom }) {
  return (
    <div className="layout-bottom">
      <div className="container">{bottom}</div>
    </div>
  );
}
