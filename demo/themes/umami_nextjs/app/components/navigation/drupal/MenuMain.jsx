'use client';

import React from 'react';
import Link from 'next/link';
import clsx from 'clsx';
import cleanClass from '@/lib/utils';
import a2p from '@/lib/a2p';

const MenuMain = ({ items, menuName, attributes }) => {
  return (
    <MenuLinks
      items={items}
      menuLevel={0}
      menuName={menuName}
      attributes={attributes}
    />
  );
};

function renderItems(items, attributes, menuLevel, menuName) {
  // workaround to get path value
  const titleToPathMap = {
    Articles: 'articles',
    Recipes: 'recipes',
    Home: '<front>',
  };
  return Object.keys(items).map((key, index) => {
    const item = items[key];
    const itemClasses = [
      `menu-${cleanClass(menuName)}__item`,
      item.isExpanded ? `menu-${cleanClass(menuName)}__item--expanded` : '',
      item.isCollapsed ? `menu-${cleanClass(menuName)}__item--collapsed` : '',
      item.inActiveTrail
        ? `menu-${cleanClass(menuName)}__item--active-trail`
        : '',
      'mt-3 text-center md:mt-0 md:mb-0',
      'mb-4 font-scopeOne font-semibold leading-snug',
    ];
    const linkClasses = [
      `menu-${cleanClass(menuName)}__link`,
      'inline-block m-1 px-0.5 pb-px transition-all duration-200 no-underline text-current border-b ' +
        'border-transparent border-b-2 bg-inherit hover:text-orange-600 hover:border-orange-600 ' +
        'focus:text-orange-600 focus:border-orange-600 active:border-orange-600',
    ];
    return (
      <li key={index} {...a2p(item.attributes, { class: clsx(itemClasses) })}>
        <Link
          href={item.url}
          className={clsx(linkClasses)}
          data-drupal-link-system-path={titleToPathMap[item.title]}
        >
          {item.title}
        </Link>
        {item.below && (
          <MenuLinks
            items={item.below}
            attributes={attributes}
            menuLevel={menuLevel + 1}
            menuName={menuName}
          />
        )}
      </li>
    );
  });
}

const MenuLinks = ({ items, attributes, menuLevel, menuName }) => {
  const menuClasses = [
    `menu-${cleanClass(menuName)}`,
    'overflow-hidden max-h-0 m-0 p-0 list-none transition-max-height duration-500 ease-in text-black font-serif text-lg font-normal leading-snug',
    'md:flex md:overflow-auto md:flex-wrap md:justify-end md:max-h-full',
  ];
  const submenuClasses = [`menu-${cleanClass(menuName)}__submenu`];
  if (items && Object.keys(items).length > 0) {
    return menuLevel === 0 ? (
      <ul
        {...a2p(attributes, { class: clsx(menuClasses) })}
        data-drupal-selector="menu-main"
      >
        {renderItems(items, attributes, menuLevel, menuName)}
      </ul>
    ) : (
      <ul {...a2p(attributes, { class: clsx(submenuClasses) })}>
        {renderItems(items, attributes, menuLevel, menuName)}
      </ul>
    );
  }
  return null;
};

export default MenuMain;
