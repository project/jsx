'use client';

import Image from 'next/image';
import { useRef, useState } from 'react';
import clsx from 'clsx';
import cleanClass from '@/lib/utils';
import a2p from '@/lib/a2p';

export default function BlockUmamiMainMenu({
  derivativePluginId,
  configuration,
  content,
  attributes,
  titleAttributes,
  titlePrefix,
  titleSuffix,
}) {
  const [isActive, setIsActive] = useState(false);
  const navClasses = [
    'block',
    'block-menu',
    'navigation',
    derivativePluginId ? `menu-${cleanClass(derivativePluginId)}__wrapper` : '',
    'flex-[0_1] text-center md:text-right',
  ];
  const headingId = `${attributes.id || ''}-menu`;
  const buttonElement = useRef(null);
  function onClickHandler() {
    buttonElement.current.classList.toggle('menu-main-toggle--active');
    const mainMenu = document.querySelector(
      '[data-drupal-selector="menu-main"]',
    );
    mainMenu.classList.toggle('menu-main--active');
    setIsActive(!isActive);
  }

  return (
    <>
      <div className="menu-main-togglewrap md:hidden">
        <button
          type="button"
          name="menu_toggle"
          className="menu-main-toggle w-10 h-10 mr-[-9px] px-1.5 text-left border-3 border-transparent rounded-none bg-transparent leading-none hover:bg-transparent md:hidden"
          data-drupal-selector="menu-main-toggle"
          aria-label="Toggle the menu"
          onClick={onClickHandler}
          ref={buttonElement}
        >
          <Image
            src="/menu-icon.svg"
            alt="menu-icon"
            className="block"
            height={20}
            width={20}
          />
        </button>
      </div>
      <nav
        role="navigation"
        aria-labelledby={headingId}
        style={{ flexBasis: `calc(100% - 200px)` }}
        {...a2p(attributes, { class: clsx(navClasses) })}
      >
        <>
          {titlePrefix}
          <h2
            {...a2p(
              titleAttributes,
              configuration.label_display === true
                ? ''
                : { class: 'visually-hidden' },
            )}
            id={headingId}
          >
            {configuration.label}
          </h2>
          {titleSuffix}
        </>
        {content}
      </nav>
    </>
  );
}
