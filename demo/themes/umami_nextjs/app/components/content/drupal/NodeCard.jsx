import React from 'react';
import Link from 'next/link';
import Image from 'next/image';
import clsx from 'clsx';
import a2p from '@/lib/a2p';
import cleanClass from '@/lib/utils';

export default function NodeCard({
  node,
  viewMode,
  attributes,
  label,
  titlePrefix,
  titleSuffix,
  url,
  content,
  titleAttributes,
  contentAttributes,
}) {
  const articleClasses = [
    'node',
    `node--type-${node.bundle}`,
    node.isPromoted && 'node--promoted',
    node.isSticky && 'node--sticky',
    !node.isPublished && 'node--unpublished',
    `node--view-mode-${cleanClass(viewMode)}`,
    'view-mode-card',
    'flex flex-col w-full p-4 border-[1px] border-[solid] border-[#fcece7] bg-[#fff]',
  ];

  const pointerStyles =
    'content-[""] absolute top-1/4 right-[0] h-[14px] bg-no-repeat bg-[0_0] bg-contain block';
  const linkStyles =
    'relative inline-block box-border pr-[20px] uppercase border-b-[1px_solid_transparent] [background:inherit] hover:no-underline text-[#008068] border-b-[1px_solid_#008068] bg-transparent focus:no-underline text-[#008068] border-b-[1px_solid_#008068] bg-transparent';

  const nodeLinkContent = () => (
    <Link className={clsx('read-more__link', linkStyles)} href={url}>
      View {node.bundle}
      <Image
        className={pointerStyles}
        src="/pointer.svg"
        alt="Pointer"
        height={12}
        width={12}
      />
      <span className="visually-hidden"> - {label}</span>
    </Link>
  );

  const nodeCard = () => (
    <>
      <div {...a2p(contentAttributes, { class: 'node__content' })}>
        {content}
      </div>
      <div className="read-more">{nodeLinkContent()}</div>
    </>
  );

  const nodeAltCard = () => (
    <>
      <div className={clsx('read-more', viewMode === 'card_common' && 'mb-4')}>
        {nodeLinkContent()}
      </div>
      <div
        {...a2p(contentAttributes, {
          class: clsx(
            'node__content',
            viewMode === 'card_common_alt' && 'order-first',
          ),
        })}
      >
        {content}
      </div>
    </>
  );

  return (
    <article {...a2p(attributes, { class: articleClasses })}>
      {titlePrefix}
      <h2 {...a2p(titleAttributes, { class: 'flex-grow' })}>{label}</h2>
      {titleSuffix}

      {viewMode === 'card' ? nodeCard() : nodeAltCard()}
    </article>
  );
}
