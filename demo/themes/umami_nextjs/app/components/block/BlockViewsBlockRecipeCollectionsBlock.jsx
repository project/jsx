import Block from './Block';

export default function BlockViewsBlockRecipeCollectionsBlock(props) {
  return (
    <div className="container !max-w-full">
      <Block {...props}></Block>
    </div>
  );
}
