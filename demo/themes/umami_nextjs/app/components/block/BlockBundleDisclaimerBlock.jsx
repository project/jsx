import Block from './Block';

export default function BlockBundleDisclaimerBlock(props) {
  const disclaimer = (
    <div className="disclaimer lg:flex justify-between">
      {props.content.fieldDisclaimer && (
        <div className="disclaimer__disclaimer block text-center lg:text-[0.94rem] lg:mb-0 lg:max-w-[40%] lg:ml-2 lg:mr-2 lg:ml-0 lg:text-start">
          {props.content.fieldDisclaimer}
        </div>
      )}
      {props.content.fieldCopyright && (
        <div className="disclaimer__copyright block text-center lg:text-[0.94rem] lg:mb-0 lg:w-1/4 lg:text-start">
          {props.content.fieldCopyright}
        </div>
      )}
    </div>
  );

  return <Block {...props} content={disclaimer}></Block>;
}
