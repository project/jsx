import React from 'react';
import a2p from '@/lib/a2p';
import cleanClass from '@/lib/utils';

export default function BlockFooterPromoBlock({
  label,
  titleAttributes,
  attributes,
  content,
  titlePrefix,
  titleSuffix,
  elements,
}) {
  const bundleClass = cleanClass(
    `block-type-${elements.content.blockContent.bundle}`,
  );

  return (
    <div {...a2p(attributes, { class: bundleClass })}>
      {content.fieldMediaImage}
      {titlePrefix}
      {label ? (
        <h2 {...a2p(titleAttributes)}>{label}</h2>
      ) : (
        content.fieldTitle && (
          <h2 {...a2p(titleAttributes)}>{content.fieldTitle}</h2>
        )
      )}
      {titleSuffix}
      <div className="footer-promo-content">
        {label
          ? Object.keys(content).map((key) => {
              return (
                <div key={key}>{key !== 'fieldMediaImage' && content[key]}</div>
              );
            })
          : Object.keys(content).map((key) => {
              return (
                <div key={key}>
                  {key !== 'fieldMediaImage' &&
                    key !== 'fieldTitle' &&
                    content[key]}
                </div>
              );
            })}
      </div>
    </div>
  );
}
