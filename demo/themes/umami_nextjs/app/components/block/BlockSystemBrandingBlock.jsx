import Link from 'next/link';
import Image from 'next/image';
import Block from './Block';

export default function BlockSystemBrandingBlock(props) {
  const branding = (
    <div className="branding">
      {props.siteLogo && (
        <Link href="/" rel="home" className="branding__site-logo">
          <Image src="/logo.svg" width={190} height={50} alt="Home" />
        </Link>
      )}
      {props.siteName && (
        <div className="branding__site-name">
          <Link href="/" rel="home">
            {props.siteName}
          </Link>
        </div>
      )}
      {props.siteSlogan && (
        <div className="branding__site-slogan">{props.siteSlogan}</div>
      )}
    </div>
  );

  return <Block {...props} content={branding}></Block>;
}
