import clsx from 'clsx';
import a2p from '@/lib/a2p';
import cleanClass from '@/lib/utils';

export default function Block({
  configuration,
  pluginId,
  attributes,
  titlePrefix,
  titleAttributes,
  label,
  titleSuffix,
  content,
  elements,
}) {
  // umami_preprocess_block
  function getBundleClass() {
    if (elements && elements.content && elements.content.blockContent) {
      const { bundle } = elements.content.blockContent;
      return cleanClass(`block-type-${bundle}`);
    }
  }
  const classes = clsx(
    'block',
    `block-${cleanClass(configuration.provider)}`,
    `block-${cleanClass(pluginId)}`,
    getBundleClass(),
  );

  return (
    <div {...a2p(attributes, { class: classes })}>
      {titlePrefix}
      {label && (
        <h2 {...a2p(titleAttributes, { class: 'block__title' })}>{label}</h2>
      )}
      {titleSuffix}
      {content}
    </div>
  );
}
