import clsx from 'clsx';
import a2p from '@/lib/a2p';
import cleanClass from '@/lib/utils';

export default function BlockBundleBannerBlock({
  configuration,
  pluginId,
  titlePrefix,
  label,
  titleAttributes,
  titleSuffix,
  content,
  elements,
  attributes,
}) {
  // @todo: Find way to simplify drupal object structure so accessing values is easier.
  const imagePath =
    content.fieldMediaImage[0].media.fieldMediaImage.entity.uri.value.replace(
      /^public:\/\//,
      '/sites/default/files/',
    );
  const bundleClass = cleanClass(
    `block-type-${elements.content.blockContent.bundle}`,
  );
  const classes = clsx(
    'block',
    `block-${cleanClass(configuration.provider)}`,
    `block-${cleanClass(pluginId)}`,
    'cover-image',
    bundleClass,
  );

  return (
    <div
      {...a2p(attributes, { class: classes })}
      style={{
        backgroundImage: `url("${process.env.NEXT_PUBLIC_DRUPAL_BASE_URL}${imagePath}")`,
      }}
    >
      <div className="block-inner">
        {titlePrefix}
        {label && <h2 {...titleAttributes}>{label}</h2>}
        {titleSuffix}
        <div className="content" style={{ display: 'flex' }}>
          <div className="summary">
            {Object.keys(content)
              .reverse()
              .map((key) => {
                return (
                  <div key={key}>
                    {key !== 'fieldMediaImage' &&
                      key !== 'blockContent' &&
                      content[key]}
                  </div>
                );
              })}
          </div>
        </div>
      </div>
    </div>
  );
}
