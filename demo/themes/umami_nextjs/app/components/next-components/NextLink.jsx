import Link from 'next/link';

export default function NextLink(props) {
  return (
    <Link href={props.href} className={props.class}>
      {props.children}
    </Link>
  );
}
