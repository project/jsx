import Image from 'next/image';

export default function NextImage(props) {
  return (
    <Image
      src={`${process.env.NEXT_PUBLIC_DRUPAL_BASE_URL}${props.src}`}
      srcSet={props.srcset}
      width={props.width}
      height={props.height}
      className={props.class}
      alt={props.alt}
      loading={props.loading}
      sizes={props.sizes}
    />
  );
}
