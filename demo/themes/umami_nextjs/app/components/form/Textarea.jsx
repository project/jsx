'use client';

import a2p from '@/lib/a2p';
import inputBehaviors from './inputBehaviors';

function Textarea({ attributes = {}, wrapperAttributes = {} }) {
  return (
    <div {...a2p(wrapperAttributes)}>
      <textarea {...a2p(attributes)} />
    </div>
  );
}

export default inputBehaviors(Textarea);
