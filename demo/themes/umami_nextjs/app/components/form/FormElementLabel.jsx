import clsx from 'clsx';
import a2p from '@/lib/a2p';

export default function FormElementLabel({
  title = '',
  titleDisplay = '',
  required = '',
  attributes = {},
}) {
  const classes = clsx(
    'form-label',
    titleDisplay === 'after' ? 'option' : '',
    titleDisplay === 'invisible' ? 'visually-hidden' : '',
    required ? 'js-form-required' : '',
    required ? 'form-required' : '',
  );
  const show = !!title || !!required;
  return (
    show && (
      <label {...a2p(attributes, { class: classes })}>{title['#markup']}</label>
    )
  );
}
