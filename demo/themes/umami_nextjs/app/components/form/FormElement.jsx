import clsx from 'clsx';
import a2p from '@/lib/a2p';

export default function FormElement({
  attributes = {},
  errors = '',
  prefix = '',
  suffix = '',
  required = false,
  type = '',
  name,
  label = '',
  labelDisplay = '',
  description = {},
  descriptionDisplay,
  disabled = '',
  titleDisplay,
  children = '',
  renderChildren = '',
}) {
  const classes = clsx(
    'js-form-item',
    'form-item',
    `js-form-type-${type}`,
    `form-type-${type}`,
    `js-form-item-${name}`,
    `form-item-${name}`,
    !['after', 'before'].includes(titleDisplay) ? 'form-no-label' : '',
    disabled === 'disabled' ? 'form-disabled' : '',
    errors ? 'form-item--error' : '',
  );

  const descriptionClasses = clsx(
    'description',
    descriptionDisplay === 'invisible' ? 'visually-hidden' : '',
  );

  const descriptionElement = () => (
    <div
      {...a2p(description.attributes || {}, {
        class: descriptionClasses,
      })}
    >
      {description.content}
    </div>
  );

  return (
    <div {...a2p(attributes, { class: classes })}>
      {['before', 'invisible'].includes(labelDisplay) && label}
      {prefix.length > 0 && <span class="field-prefix">{prefix}</span>}
      {descriptionDisplay === 'before' &&
        description.content &&
        descriptionElement()}
      {renderChildren}

      {suffix.length > 0 && <span class="field-suffix">{suffix}</span>}
      {['after'].includes(labelDisplay) && label}

      {errors && (
        <div class="form-item--error-message form-item-errors">{errors}</div>
      )}
      {['after', 'invisible'].includes(descriptionDisplay) &&
        description.content &&
        descriptionElement()}
    </div>
  );
}
