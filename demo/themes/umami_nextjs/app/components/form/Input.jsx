'use client';

import a2p from '@/lib/a2p';
import inputBehaviors from './inputBehaviors';

function Input(properties) {
  const { attributes = {}, children = '', renderChildren = '' } = properties;
  return (
    <>
      <input {...a2p(attributes)} />
      {renderChildren}
    </>
  );
}

export default inputBehaviors(Input);
