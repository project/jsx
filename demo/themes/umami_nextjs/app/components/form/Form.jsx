'use client';

import { createContext, useState } from 'react';
import a2p from '@/lib/a2p';

export const DrupalFormContext = createContext({});
export const DrupalFormDispatchContext = createContext({});

export default function Form({
  attributes = {},
  children = '',
  renderChildren = '',
}) {
  const [formState, setFormState] = useState({
    formId: attributes['data-drupal-selector'] || '',
  });
  return (
    <DrupalFormContext.Provider value={{ formState, setFormState }}>
      <form {...a2p(attributes)}>{renderChildren}</form>
    </DrupalFormContext.Provider>
  );
}
