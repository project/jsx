import { Window } from 'happy-dom';

// html to JSX
export default function serverSideHyperscriptify(
  domElementOrFragment,
  h,
  Fragment,
  components,
  options = {},
) {
  const window = new Window();
  // Collect basic info about the element or fragment.
  let element;
  let tagName;
  let component;
  let attributes;
  let childNodes;
  switch (domElementOrFragment.nodeType) {
    case window.Node.ELEMENT_NODE:
      element = domElementOrFragment;
      tagName = element.nodeName.toLowerCase();
      component = components[tagName];
      attributes = Object.fromEntries(
        element
          .getAttributeNames()
          .map((name) => [name, element.getAttribute(name)]),
      );
      childNodes = element.childNodes;
      break;
    case window.Node.DOCUMENT_FRAGMENT_NODE:
      component = Fragment;
      attributes = {};
      childNodes = domElementOrFragment.childNodes;
      break;
    default:
      return;
  }

  // Collect the element's or fragment's slots and children.
  const slots = {};
  const children = [];
  childNodes.forEach(function (childNode) {
    let child;
    switch (childNode.nodeType) {
      case window.Node.TEXT_NODE:
        child = childNode.nodeValue;
        break;
      case window.Node.ELEMENT_NODE: {
        const slot = childNode.getAttribute('slot');
        child = serverSideHyperscriptify(
          childNode,
          h,
          Fragment,
          components,
          options,
        );
        if (element && component && slot) {
          slots[slot] = child;
          child = null;
        }
        break;
      }
      default:
        break;
    }
    if (child) {
      children.push(child);
    }
  });

  // For elements that have a slot attribute, and are child elements of a
  // component, this function uses that slot attribute to distribute that slot
  // content to the parent component's props (see the code above). Therefore,
  // don't also retain the slot attribute, since that could conflict with the
  // component's props expectations or trigger additional slot distribution by
  // the browser.
  if (
    attributes.slot &&
    components[element.parentNode.nodeName.toLowerCase()]
  ) {
    delete attributes.slot;
  }

  // Create a props object from the attributes and slots.
  const context = { tagName, component, element };
  const props =
    element && options.propsify
      ? options.propsify(attributes, slots, context)
      : { ...attributes, ...slots };

  return h(component || tagName, props, ...children);
}
