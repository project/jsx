import propsify from '@drupal-jsx/propsify/strict';

export default function a2p(
  attributesFromDrupal,
  attributesFromComponent = {},
) {
  const combinedAttributes = {};
  const attributeNames = new Set(
    Object.keys(attributesFromDrupal).concat(
      Object.keys(attributesFromComponent),
    ),
  );
  attributeNames.forEach((name) => {
    let value;
    if (
      Array.isArray(attributesFromDrupal[name]) &&
      Array.isArray(attributesFromComponent[name])
    ) {
      value = attributesFromDrupal[name].concat(attributesFromComponent[name]);
    } else {
      value = attributesFromComponent[name] ?? attributesFromDrupal[name];
    }

    if (Array.isArray(value)) {
      value = value.join(' ');
    }
    combinedAttributes[name] = value;
  });

  return propsify(combinedAttributes, {}, {});
}
