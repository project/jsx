import propsify from '@drupal-jsx/propsify/strict';
import { createElement as h, Fragment } from 'react';
import serverSideHyperscriptify from '@/lib/serverSideHyperscriptify';
import components from '@/components';

const slots = [
  'page.preHeader',
  'page.header',
  'page.highlighted',
  'page.contentBottom',
  'page.footer',
  'page.bottom',
];

// Store header and footer in global object and return page element with
// main content.
export default function cacheFromSlots(pageElement) {
  slots.forEach((slotName) => {
    const slotElement = pageElement.querySelector(`[slot="${slotName}"]`);
    global.slotCache[slotName] = serverSideHyperscriptify(
      slotElement,
      h,
      Fragment,
      components,
      { propsify },
    );
    slotElement.remove();
  });
  return pageElement;
}
