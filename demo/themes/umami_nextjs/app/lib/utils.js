// Replacement for Drupal's clean_class function.
export default function cleanClass(input) {
  input = input.replace(/[\s_]+/g, '-').toLowerCase();
  input = input.replace(/[^\w-]/g, '');
  return input;
}
