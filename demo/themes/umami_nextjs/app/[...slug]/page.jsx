import { createElement as h, Fragment } from 'react';
import { Window } from 'happy-dom';
import propsify from '@drupal-jsx/propsify/strict';
import serverSideHyperscriptify from '@/lib/serverSideHyperscriptify';
import components from '../components';
import getData from '@/lib/getData';
import cacheFromSlots from '@/lib/cacheFromSlots';

export default async function Page({ params, searchParams }) {
  // @todo: Update URL construction to include searchParams
  const formatSlug = (slugArray) =>
    Array.isArray(slugArray) ? `${slugArray.join('/')}` : '';
  const url = `${process.env.NEXT_PUBLIC_DRUPAL_BASE_URL}/${formatSlug(
    params.slug,
  )}`;
  const response = await getData(url);
  const window = new Window();
  const { document } = window;
  document.body.innerHTML = `
     ${response}
`;
  const pageElement = document
    .querySelector('template')
    .content.querySelector('drupal-page');

  if (!global.slotCache) {
    global.slotCache = {};
  }

  const App = serverSideHyperscriptify(
    cacheFromSlots(pageElement),
    h,
    Fragment,
    components,
    {
      propsify,
    },
  );

  return App;
}
