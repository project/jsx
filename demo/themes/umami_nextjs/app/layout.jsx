import { Window } from 'happy-dom';
// These styles apply to every route in the application
import './globals.css';
import Script from 'next/script';
import getData from '@/lib/getData';
import {
  PageBottom,
  PageContentBottom,
  PageFooter,
  PageHeader,
  PageHighlighted,
} from '@/components/layout/drupal/Page';

export const metadata = {
  title: 'Umami Next.js',
  description: 'JSX Theme Engine for Umami',
};

export default async function RootLayout({ children }) {
  const response = await getData(process.env.NEXT_PUBLIC_DRUPAL_BASE_URL);
  const window = new Window();
  const { document } = window;
  document.body.innerHTML = `
     ${response}
`;
  const linkTags = document.getElementsByTagName('link');
  const scriptTags = document.getElementsByTagName('script');

  // Add Drupal scripts
  const scripts = scriptTags.map((s, index) => {
    const scriptProps = { id: index };
    if (s.hasAttribute('data-drupal-selector')) {
      scriptProps.dataDrupalSelector = s.getAttribute('data-drupal-selector');
      scriptProps.type = s.getAttribute('type');
      // Loads script before any next.js code and page hydration
      scriptProps.strategy = 'beforeInteractive';
      return (
        <Script key={index} {...scriptProps}>
          {s.innerHTML}
        </Script>
      );
    }
    scriptProps.src = process.env.NEXT_PUBLIC_DRUPAL_BASE_URL + s.src;
    scriptProps.strategy = 'beforeInteractive';
    return <Script key={index} {...scriptProps} />;
  });

  const {
    'page.header': header,
    'page.preHeader': preHeader,
    'page.highlighted': highlighted,
    'page.contentBottom': contentBottom,
    'page.footer': footer,
    'page.bottom': bottom,
  } = global.slotCache;

  return (
    <html lang="en">
      <head>
        {linkTags.map((l, index) => (
          <link
            key={index}
            rel="stylesheet"
            media="all"
            href={`${process.env.NEXT_PUBLIC_DRUPAL_BASE_URL}${l.href}`}
          />
        ))}
      </head>
      <body>
        <div className="layout-container">
          <PageHeader preHeader={preHeader} header={header}></PageHeader>
          <PageHighlighted highlighted={highlighted}></PageHighlighted>
          {children}
          <PageContentBottom contentBottom={contentBottom}></PageContentBottom>
          <PageFooter footer={footer}></PageFooter>
          <PageBottom bottom={bottom}></PageBottom>
        </div>
        {scripts}
      </body>
    </html>
  );
}
