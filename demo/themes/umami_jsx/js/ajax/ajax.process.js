
(function (Drupal) {
  Drupal.behaviors.jsxAjaxProcess = {
    attach(context, settings) {
      const addressDuplicateIds = () => {
        const ids = [...document.querySelectorAll('[id]')]
          .filter(el => !el.closest('drupal-html-fragment'))
          .map(v => v.id)
          .reduce((acc, v) => { acc[v] = (acc[v] || 0) + 1; return acc }, {});
        Object.entries(ids)
          .filter(([key, value]) => value > 1)
          .map(([ key, value]) => key)
          .forEach((dupeId) => {
            [...document.querySelectorAll(`[id="${dupeId}"]`)].forEach((element, index) => {
              if (index !== 0) {
                element.id = `${dupeId}-${new Date().getTime()}`
              }
            })
          })
      }

      addressDuplicateIds();

      // After hyperscriptifying a context, we send it through Drupal
      // behaviors. The doNotReinvoke flag indicates already-scriptified
      // content that does not need to proceed further.
      if (settings.doNotReinvoke) {
        return;
      }
      // If the JSX theme engine isn't yet up and running, go no further.
      if(!Drupal.JSXComponents) {
        return;
      }

      context.querySelectorAll("drupal-html-fragment").forEach((fragment) => {
        setTimeout(() => {
          // Clean out drupal html fragments after they've been scriptified so there
          // aren't matching selectors for elements that have already served
          // their purpose in guiding the scriptification process.
          if (fragment.hasAttribute('data-drupal-scriptified')) {
            fragment.innerHTML = '';
          }
        }, 20)
      })


      Object.keys(Drupal.JSXComponents).forEach(componentName => {
        // If the top-level element in context is a JSX component
        if (context.tagName && context.tagName.toLowerCase() === componentName) {
          if (!context.tagName.toLowerCase().includes('fragment') && !context.hasAttribute('data-drupal-scriptified')) {
            const container =  Drupal.JSXAdditional(Drupal.JSXHyperscriptify(context), context);
            context.hidden = true;
            context.setAttribute('data-drupal-scriptified', true)
            setTimeout(() => {
              Drupal.attachBehaviors(container, {...settings, doNotReinvoke: true});
            })
          }
          [...context.querySelectorAll('[id]')]
            .forEach(element => {
              if (document.querySelectorAll(`[id="${element.id}"]`).length > 1) {
                element.id = `${element.id}-${new Date().getTime()}`
              }
            })
        } else {
          // Otherwise, search for the component inside a context.
         [...context.querySelectorAll(`${componentName}:not([data-drupal-scriptified])`)].forEach(component => {
           if (!component.hasAttribute('data-drupal-scriptified')) {
             const container =  Drupal.JSXAdditional(Drupal.JSXHyperscriptify(component),component);
             component.setAttribute('data-drupal-scriptified', true)
             component.hidden = true;
             // @todo: it is probably necessary to have a Drupal.attachBehaviors(container, {...settings, doNotReinvoke: true});
             // here so it isn't only happening when the top-level parent of context is a JSX component.
             // It isn't here currently as it was causing duplicate renders. It is possible this is no longer
             // a problem due to fixes made elsewhere.
           }
         })

        }
      })
    }
  }

})( Drupal);
