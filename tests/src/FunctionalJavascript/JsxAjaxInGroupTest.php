<?php

namespace Drupal\Tests\jsx\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\Ajax\AjaxInGroupTest;


/**
 * Tests that form elements in groups work correctly with AJAX.
 *
 * @group Ajax
 */
class JsxAjaxInGroupTest extends AjaxInGroupTest {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['sdc'];

  protected $profile = 'demo_umami';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    $this->markTestSkipped('we need to get `replaceWith` working').
    parent::setUp();
    $this->container->get('theme_installer')->install(['umami_jsx']);
    $this->config('system.theme')->set('default', 'umami_jsx')->save();
  }

}
