<?php

namespace Drupal\Tests\jsx\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\Ajax\FormValuesTest;

/**
 * Tests that form values are properly delivered to AJAX callbacks.
 *
 * @group Ajax
 */
class JsxFormValuesTest extends FormValuesTest {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['sdc'];

  protected $profile = 'demo_umami';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    $this->markTestSkipped('these tests pass locally but fail randomly in different spots on gitlab CI');
    parent::setUp();
    $this->container->get('theme_installer')->install(['umami_jsx']);
    $this->config('system.theme')->set('default', 'umami_jsx')->save();
  }

}
