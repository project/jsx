<?php

namespace Drupal\Tests\jsx\FunctionalJavascript;

use Drupal\Core\Url;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\FunctionalJavascriptTests\Ajax\AjaxFormCacheTest;


/**
 * Tests the usage of form caching for AJAX forms.
 *
 * @group Ajax
 */
class JsxAjaxFormCacheTest extends AjaxFormCacheTest {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['sdc'];

  protected $profile = 'demo_umami';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->container->get('theme_installer')->install(['umami_jsx']);
    $this->config('system.theme')->set('default', 'umami_jsx')->save();
  }

  public function testFormCacheUsage() {
    $this->markTestSkipped('this test seems to just hang and not proceed?');
  }

  /**
   * Tests AJAX forms in blocks.
   */
  public function testBlockForms() {
    $this->markTestSkipped('This test passes locally, but intermittently fails on CI, usually selecting options and waiting for the changes.');
    $this->container->get('module_installer')->install(['block', 'search']);
    $this->rebuildContainer();
    $this->drupalLogin($this->rootUser);

    $this->drupalPlaceBlock('search_form_block', ['weight' => -5]);
    $this->drupalPlaceBlock('ajax_forms_test_block');

    $this->drupalGet('');
    $session = $this->getSession();
    $session->wait(200);

    // Select first option and trigger ajax update.
    $session->getPage()->selectFieldOption('edit-test1', 'option1');

    // DOM update: The InsertCommand in the AJAX response changes the text
    // in the option element to 'Option1!!!'.
    $opt1_selector = $this->assertSession()->waitForElement('css', "select[data-drupal-selector='edit-test1'] option:contains('Option 1!!!')");
    $this->assertNotEmpty($opt1_selector);
    $this->assertTrue($opt1_selector->isSelected());

    // Confirm option 3 exists.
    $page = $session->getPage();
    $opt3_selector = $page->find('xpath', '//select[@data-drupal-selector="edit-test1"]//option[@value="option3"]');
    $this->assertNotEmpty($opt3_selector);
    $session->wait(300);

    // Confirm success message appears after a submit.
    $page->find('css', '#ajax-forms-test-block [data-drupal-selector="edit-submit"]')->click();
    $this->assertSession()->waitForButton('edit-submit');
    $updated_page = $session->getPage();
    $updated_page->hasContent('Submission successful.');
  }

  /**
   * Tests AJAX forms on pages with a query string.
   */
  public function testQueryString() {
    $this->container->get('module_installer')->install(['block']);
    $this->drupalLogin($this->rootUser);

    $this->drupalPlaceBlock('ajax_forms_test_block');

    $url = Url::fromRoute('entity.user.canonical', ['user' => $this->rootUser->id()], ['query' => ['foo' => 'bar']]);
    $this->drupalGet($url);

    $session = $this->getSession();
    $session->wait(200);
    // Select first option and trigger ajax update.
    $session->getPage()->selectFieldOption('edit-test1', 'option1');

    // DOM update: The InsertCommand in the AJAX response changes the text
    // in the option element to 'Option1!!!'.
    $opt1_selector = $this->assertSession()->waitForElement('css', "option:contains('Option 1!!!')");
    $this->assertNotEmpty($opt1_selector);

    $url->setOption('query', [
      'foo' => 'bar',
    ]);
    $this->assertSession()->addressEquals($url);
  }

}

