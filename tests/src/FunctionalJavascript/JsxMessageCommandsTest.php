<?php

namespace Drupal\Tests\jsx\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\Ajax\MessageCommandTest;

/**
 * Performs tests on AJAX framework commands.
 *
 * @group Ajax
 */
class JsxMessageCommandsTest extends MessageCommandTest {
  /**
   * {@inheritdoc}
   */
  protected static $modules = ['sdc'];

  protected $profile = 'demo_umami';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->container->get('theme_installer')->install(['umami_jsx']);
    $this->config('system.theme')->set('default', 'umami_jsx')->save();
  }

  public function testMessageCommand() {
    $page = $this->getSession()->getPage();
    $assert_session = $this->assertSession();

    $this->drupalGet('ajax-test/message');
    $page->pressButton('Make Message In Default Location');
    $this->waitForMessageVisible('I am a message in the default location.');
    $this->assertAnnounceContains('I am a message in the default location.');
    $assert_session->elementsCount('css', '.messages__wrapper .messages', 1);

    $page->pressButton('Make Message In Alternate Location');
    $this->waitForMessageVisible('I am a message in an alternate location.', '#alternate-message-container');
    $assert_session->pageTextContains('I am a message in the default location.');
    $this->assertAnnounceContains('I am a message in an alternate location.');
    $assert_session->elementsCount('css', '.messages__wrapper .messages', 1);
    $assert_session->elementsCount('css', '#alternate-message-container .messages', 1);

    $page->pressButton('Make Warning Message');
    $this->waitForMessageVisible('I am a warning message in the default location.', NULL, 'warning');
    $assert_session->pageTextNotContains('I am a message in the default location.');
    $assert_session->elementsCount('css', '.messages__wrapper .messages', 1);
    $assert_session->elementsCount('css', '#alternate-message-container .messages', 1);

    $this->drupalGet('ajax-test/message');
    $this->getSession()->wait(300);
    // Test that by default, previous messages in a location are removed.
    for ($i = 0; $i < 6; $i++) {
      $page->pressButton('Make Message In Default Location');
      $this->waitForMessageVisible('I am a message in the default location.');
      $assert_session->elementsCount('css', '.messages__wrapper .messages', 1);

      $page->pressButton('Make Warning Message');
      $this->waitForMessageVisible('I am a warning message in the default location.', NULL, 'warning');
      // Test that setting MessageCommand::$option['announce'] => '' suppresses
      // screen reader announcement.
      $this->assertAnnounceNotContains('I am a warning message in the default location.');
      $this->waitForMessageRemoved('I am a message in the default location.');
      $assert_session->elementsCount('css', '.messages__wrapper .messages', 1);
    }

    // Test that if MessageCommand::clearPrevious is FALSE, messages will not
    // be cleared.
    $this->drupalGet('ajax-test/message');
    for ($i = 1; $i < 7; $i++) {
      $page->pressButton('Make Message In Alternate Location');
      $this->getSession()->wait(300);
      $expected_count = count($page->findAll('css', '#alternate-message-container .messages')) === $i;
      $this->assertTrue($expected_count, "Failed on step $i, count was $expected_count");
      $this->assertAnnounceContains('I am a message in an alternate location.');
    }
  }

}
