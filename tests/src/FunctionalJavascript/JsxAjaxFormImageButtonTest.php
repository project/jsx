<?php

namespace Drupal\Tests\jsx\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\Ajax\AjaxFormImageButtonTest;

/**
 * Tests the Ajax image buttons work with key press events.
 *
 * @group Ajax
 */
class JsxAjaxFormImageButtonTest extends AjaxFormImageButtonTest {
  /**
   * {@inheritdoc}
   */
  protected static $modules = ['sdc'];

  protected $profile = 'demo_umami';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->container->get('theme_installer')->install(['umami_jsx']);
    $this->config('system.theme')->set('default', 'umami_jsx')->save();
  }
}
