<?php

namespace Drupal\Tests\jsx\FunctionalJavascript;

use Drupal\Component\Utility\UrlHelper;
use Drupal\FunctionalJavascriptTests\Ajax\AjaxTest;

/**
 * Performs tests on AJAX framework commands.
 *
 * @group Ajax
 */
class JsxAjaxTest extends AjaxTest {
  /**
   * {@inheritdoc}
   */
  protected static $modules = ['sdc'];

  protected $profile = 'demo_umami';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->container->get('theme_installer')->install(['umami_jsx']);
    $this->config('system.theme')->set('default', 'umami_jsx')->save();
  }

  public function testGlobalEvents() {
    // @todo, look into adding #test_global_events_log, #test_global_events_log2
    // from AjaxTestController to an area of the page that isn't rerendered by
    // JSX.
    $this->markTestSkipped('The DOM elements keeping track of the AJAX events might be an issue.');
  }

  public function testAjaxFocus() {
    $this->markTestSkipped('Focus management needs to be addressed holistically');
  }

  public function testDrupalSettingsCachingRegression() {
    $this->drupalGet('ajax-test/dialog');
    $assert = $this->assertSession();
    $session = $this->getSession();
    $session->wait(200);

    // Insert a fake library into the already loaded library settings.
    $fake_library = 'fakeLibrary/fakeLibrary';
    $libraries = $session->evaluateScript("drupalSettings.ajaxPageState.libraries");
    $libraries = UrlHelper::compressQueryParameter(UrlHelper::uncompressQueryParameter($libraries) . ',' . $fake_library);
    $session->evaluateScript("drupalSettings.ajaxPageState.libraries = '$libraries';");
    $ajax_page_state = $session->evaluateScript("drupalSettings.ajaxPageState");
    $libraries = UrlHelper::uncompressQueryParameter($ajax_page_state['libraries']);
    // Test that the fake library is set.
    $this->assertStringContainsString($fake_library, $libraries);

    // Click on the AJAX link.
    $this->clickLink('Link 8 (ajax)');
    $session->wait(300);

    // Test that the fake library is still set after the AJAX call.
    $ajax_page_state = $session->evaluateScript("drupalSettings.ajaxPageState");
    // Test that the fake library is set.
    $this->assertStringContainsString($fake_library, UrlHelper::uncompressQueryParameter($ajax_page_state['libraries']));

    // Reload the page, this should reset the loaded libraries and remove the
    // fake library.
    $this->drupalGet('ajax-test/dialog');
    $session->wait(200);
    $ajax_page_state = $session->evaluateScript("drupalSettings.ajaxPageState");
    $this->assertStringNotContainsString($fake_library, UrlHelper::uncompressQueryParameter($ajax_page_state['libraries']));

    // Click on the AJAX link again, and the libraries should still not contain
    // the fake library.
    $this->clickLink('Link 8 (ajax)');
    $session->wait(300);
    $ajax_page_state = $session->evaluateScript("drupalSettings.ajaxPageState");
    $this->assertStringNotContainsString($fake_library, UrlHelper::uncompressQueryParameter($ajax_page_state['libraries']));
  }

  /**
   * Tests that various AJAX responses with DOM elements are correctly inserted.
   *
   * After inserting DOM elements, Drupal JavaScript behaviors should be
   * reattached and all top-level elements of type Node.ELEMENT_NODE need to be
   * part of the context.
   */
  public function testInsertAjaxResponse() {
    $this->markTestSkipped('This test runs fine locally, but on CI often times out or elements become stale.');
    $render_single_root = [
      'pre-wrapped-div' => '<div class="pre-wrapped">pre-wrapped<script> var test;</script></div>',
      'pre-wrapped-span' => '<span class="pre-wrapped">pre-wrapped<script> var test;</script></span>',
      'pre-wrapped-whitespace' => ' <div class="pre-wrapped-whitespace">pre-wrapped-whitespace</div>' . "\n",
      'not-wrapped' => 'not-wrapped',
      'comment-string-not-wrapped' => '<!-- COMMENT -->comment-string-not-wrapped',
      'comment-not-wrapped' => '<!-- COMMENT --><div class="comment-not-wrapped">comment-not-wrapped</div>',
      'svg' => '<svg xmlns="http://www.w3.org/2000/svg" width="10" height="10"><rect x="0" y="0" height="10" width="10" fill="green"></rect></svg>',
      'empty' => '',
    ];
    $render_multiple_root_unwrap = [
      'mixed' => ' foo <!-- COMMENT -->  foo bar<div class="a class"><p>some string</p></div> additional not wrapped strings, <!-- ANOTHER COMMENT --> <p>final string</p>',
      'top-level-only' => '<div>element #1</div><div>element #2</div>',
      'top-level-only-pre-whitespace' => ' <div>element #1</div><div>element #2</div> ',
      'top-level-only-middle-whitespace-span' => '<span>element #1</span> <span>element #2</span>',
      'top-level-only-middle-whitespace-div' => '<div>element #1</div> <div>element #2</div>',
    ];

    // This is temporary behavior for BC reason.
    $render_multiple_root_wrapper = [];
    foreach ($render_multiple_root_unwrap as $key => $render) {
      $render_multiple_root_wrapper["$key--effect"] = '<div>' . $render . '</div>';
    }

    $expected_renders = array_merge(
      $render_single_root,
      $render_multiple_root_wrapper,
      $render_multiple_root_unwrap
    );

    // Checking default process of wrapping Ajax content.
    foreach ($expected_renders as $render_type => $expected) {
      $this->assertInsert($render_type, $expected);
    }

    // Checking custom ajaxWrapperMultipleRootElements wrapping.
    $custom_wrapper_multiple_root = <<<JS
    (function($, Drupal){
      Drupal.theme.ajaxWrapperMultipleRootElements = function (elements) {
        return $('<div class="my-favorite-div"></div>').append(elements);
      };
    }(jQuery, Drupal));
JS;
    $expected = '<div class="my-favorite-div"><span>element #1</span> <span>element #2</span></div>';
    $this->assertInsert('top-level-only-middle-whitespace-span--effect', $expected, $custom_wrapper_multiple_root);

    // Checking custom ajaxWrapperNewContent wrapping.
    $custom_wrapper_new_content = <<<JS
    (function($, Drupal){
      Drupal.theme.ajaxWrapperNewContent = function (elements) {
        return $('<div class="div-wrapper-forever"></div>').append(elements);
      };
    }(jQuery, Drupal));
JS;
    $expected = '<div class="div-wrapper-forever"></div>';
    $this->assertInsert('empty', $expected, $custom_wrapper_new_content);
  }

  public function assertInsert(string $render_type, string $expected, string $script = ''): void {
    // Check insert to block element.
    $this->drupalGet('ajax-test/insert-block-wrapper');
    $this->getSession()->wait(300);
    $this->getSession()->executeScript($script);
    $this->clickLink("Link html $render_type");
    $this->getSession()->wait(300);

    $this->assertWaitPageContains('<div class="ajax-target-wrapper"><div id="ajax-target">' . $expected . '</div></div>');

    $this->drupalGet('ajax-test/insert-block-wrapper');
    $this->getSession()->wait(300);

    $this->getSession()->executeScript($script);
    $this->clickLink("Link replaceWith $render_type");
    $this->getSession()->wait(300);

    $this->assertWaitPageContains('<div class="ajax-target-wrapper">' . $expected . '</div>');

    // Check insert to inline element.
    $this->drupalGet('ajax-test/insert-inline-wrapper');
    $this->getSession()->wait(300);

    $this->getSession()->executeScript($script);
    $this->clickLink("Link html $render_type");
    $this->getSession()->wait(300);

    $this->assertWaitPageContains('<div class="ajax-target-wrapper"><span id="ajax-target-inline">' . $expected . '</span></div>');

    $this->drupalGet('ajax-test/insert-inline-wrapper');
    $this->getSession()->wait(300);

    $this->getSession()->executeScript($script);
    $this->clickLink("Link replaceWith $render_type");
    $this->getSession()->wait(300);

    $this->assertWaitPageContains('<div class="ajax-target-wrapper">' . $expected . '</div>');
  }

}
