<?php

namespace Drupal\Tests\jsx\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\Ajax\AjaxFormPageCacheTest;

/**
 * Performs tests on AJAX forms in cached pages.
 *
 * @group Ajax
 */
class JsxAjaxFormPageCacheTest extends AjaxFormPageCacheTest {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['sdc'];

  protected $profile = 'demo_umami';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    $this->markTestSkipped('Tests pass locally, but run into timing issues on Gitlab CI.');
    parent::setUp();
    $this->container->get('theme_installer')->install(['umami_jsx']);
    $this->config('system.theme')->set('default', 'umami_jsx')->save();
  }

  public function testSimpleAJAXFormValue() {
    $this->drupalGet('ajax_forms_test_get_form');
    $build_id_initial = $this->getFormBuildId();

    // Changing the value of a select input element, triggers an AJAX
    // request/response. The callback on the form responds with three AJAX
    // commands:
    // - UpdateBuildIdCommand
    // - HtmlCommand
    // - DataCommand
    $session = $this->getSession();
    $session->getPage()->selectFieldOption('select', 'green');

    // Wait for the DOM to update. The HtmlCommand will update
    // #ajax_selected_color to reflect the color change.
    $green_span = $this->assertSession()->waitForElement('css', "#ajax_selected_color:contains('green')");
    $this->assertNotNull($green_span, 'DOM update: The selected color SPAN is green.');

    // Confirm the operation of the UpdateBuildIdCommand.
    $build_id_first_ajax = $this->getFormBuildId();
    $this->assertNotEquals($build_id_initial, $build_id_first_ajax, 'Build id is changed in the form_build_id element on first AJAX submission');

    // Changing the value of a select input element, triggers an AJAX
    // request/response.
    $session->getPage()->selectFieldOption('select', 'red');

    // Wait for the DOM to update.
    $red_span = $this->assertSession()->waitForElement('css', "#ajax_selected_color:contains('red')");
    $this->assertNotNull($red_span, 'DOM update: The selected color SPAN is red.');

    // Confirm the operation of the UpdateBuildIdCommand.
    $build_id_second_ajax = $this->getFormBuildId();
    $this->assertNotEquals($build_id_first_ajax, $build_id_second_ajax, 'Build id changes on subsequent AJAX submissions');

    // Emulate a push of the reload button and then repeat the test sequence
    // this time with a page loaded from the cache.
    $session->reload();
    $build_id_from_cache_initial = $this->getFormBuildId();
    $this->assertEquals($build_id_initial, $build_id_from_cache_initial, 'Build id is the same as on the first request');

    // Changing the value of a select input element, triggers an AJAX
    // request/response.
    $session->getPage()->selectFieldOption('select', 'green');

    // Wait for the DOM to update.
    $green_span2 = $this->assertSession()->waitForElement('css', "#ajax_selected_color:contains('green')");
    $this->assertNotNull($green_span2, 'DOM update: After reload - the selected color SPAN is green.');

    $build_id_from_cache_first_ajax = $this->getFormBuildId();
    $this->assertNotEquals($build_id_from_cache_initial, $build_id_from_cache_first_ajax, 'Build id is changed in the DOM on first AJAX submission');
    $this->assertNotEquals($build_id_first_ajax, $build_id_from_cache_first_ajax, 'Build id from first user is not reused');

    // Changing the value of a select input element, triggers an AJAX
    // request/response.
    $session->getPage()->selectFieldOption('select', 'red');

    // Wait for the DOM to update.
    $red_span2 = $this->assertSession()->waitForElement('css', "#ajax_selected_color:contains('red')");
    $this->assertNotNull($red_span2, 'DOM update: After reload - the selected color SPAN is red.');

    $build_id_from_cache_second_ajax = $this->getFormBuildId();
    $this->assertNotEquals($build_id_from_cache_first_ajax, $build_id_from_cache_second_ajax, 'Build id changes on subsequent AJAX submissions');

  }

}
