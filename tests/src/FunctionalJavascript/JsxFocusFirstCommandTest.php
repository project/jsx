<?php

namespace Drupal\Tests\jsx\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\Ajax\FocusFirstCommandTest;

/**
 * Tests setting focus via AJAX command.
 *
 * @group Ajax
 */
class JsxFocusFirstCommandTest extends FocusFirstCommandTest {
  /**
   * {@inheritdoc}
   */
  protected static $modules = ['sdc'];

  protected $profile = 'demo_umami';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
//    $this->markTestSkipped('just for now');
    parent::setUp();
    $this->container->get('theme_installer')->install(['umami_jsx']);
    $this->config('system.theme')->set('default', 'umami_jsx')->save();
  }

}
