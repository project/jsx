<?php

namespace Drupal\Tests\jsx\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\Ajax\ThrobberTest;

/**
 * Tests the throbber.
 *
 * @group Ajax
 */
class JsxThrobberTest extends ThrobberTest {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['sdc'];

  protected $profile = 'demo_umami';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    $this->markTestSkipped('this test is in views UI, and Umami does not display it');
    parent::setUp();
    $this->container->get('theme_installer')->install(['umami_jsx']);
    $this->config('system.theme')->set('default', 'umami_jsx')->save();
  }

}
