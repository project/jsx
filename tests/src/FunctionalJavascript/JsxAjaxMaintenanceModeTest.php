<?php

namespace Drupal\Tests\jsx\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\Ajax\AjaxMaintenanceModeTest;


/**
 * Tests maintenance message during an AJAX call.
 *
 * @group Ajax
 */
class JsxAjaxMaintenanceModeTest extends AjaxMaintenanceModeTest {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['sdc'];

  protected $profile = 'demo_umami';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->container->get('theme_installer')->install(['umami_jsx']);
    $this->config('system.theme')->set('default', 'umami_jsx')->save();
  }

  /**
   * Tests maintenance message only appears once on an AJAX call.
   */
  public function testAjaxCallMaintenanceMode(): void {
    $page = $this->getSession()->getPage();
    $assert_session = $this->assertSession();

    \Drupal::state()->set('system.maintenance_mode', TRUE);

    $this->drupalGet('ajax-test/insert-inline-wrapper');
    $this->getSession()->wait(200);
    $assert_session->pageTextContains('Target inline');
    $page->clickLink('Link html pre-wrapped-div');
    $this->getSession()->wait(300);
    $this->assertSession()->assertWaitOnAjaxRequest();
    $assert_session->pageTextContains('Operating in maintenance mode');
  }

}
