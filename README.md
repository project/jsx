## Installation
The following instructions are for testing the Umami JSX demo theme that's
within this theme engine. Alternatively, you could just do Step 1 and then
create your own theme without doing the other steps, but understanding how the
Umami JSX theme works will help you with doing that.

### Step 1: Install the code
From the root directory of your Composer-based Drupal installation:
```
composer config repositories.drupal/jsx vcs https://git.drupalcode.org/project/jsx.git
composer require drupal/jsx:^1@dev
```

### Step 2: Build the Umami JSX demo app
The following assumes that your Drupal site was installed with
[drupal/recommeded-project](https://www.drupal.org/docs/develop/using-composer/starting-a-site-using-drupal-composer-project-templates#s-drupalrecommended-project),
such that the web root of the site is the _"web"_ directory within the Composer
project root. If the web root is in a different location, you'll need to
replace `web/` with your web root.
```
cd web/themes/contrib/jsx/demo/themes/umami_jsx/app/
npm install; npm install     # yes, you need both
npm run build:react          # or you can replace "react" with "preact" or "solid"
```

### Step 3: Install a demo Umami site with the Umami JSX theme
1. Install a Drupal site the way you normally would (e.g., via Drupal's web
installer or via Drush). When selecting the installation profile, choose
_"demo_umami"_ (_"Demo: Umami Food Magazine (Experimental)"_).
2. Enable the _"JSX Theme Engine Demo"_ module.
3. On the Appearance page, install and set as default the _"Umami JSX"_ theme.
4. Navigate to the site's home page and click around to other pages. These
pages should look exactly like they normally look with the Umami theme, but
they're actually being rendered by React (or Preact or Solid per the earlier
"Step 2" section). In your browser's console log, you should see a message with
every page refresh that says which rendering library is being used.

## How it works

### The PHP side ([jsx.engine](jsx.engine))
The job of a Drupal theme engine is to implement two PHP functions:
- `ENGINE_NAME_theme()`: to find and return all templates implemented by a
theme that's using this engine.
- `ENGINE_NAME_render_template($template, $variables)`: to return the HTML
output for a given template rendered with a given set of variable values.

In the case of the JSX theme engine, these functions are named:
[jsx_theme()](jsx.engine#L11)
and [jsx_render_template()](jsx.engine#L27).

If you're familiar with React, you can think of `$template` as like a function
component (in fact, wherever you see the word _"template"_ in the rest of this
document, you can think of it as synonymous with _"component"_), and
`$variables` as like the component's props, and `jsx_render_template()` as like
React's internal function that calls the right function component with the
right props and renders the result to the DOM (or in the case of SSR, to an
HTML string).

However, `jsx_render_template()` doesn't operate in React or in JavaScript, it
operates in PHP, so it needs to render the template in a way that can get
converted to a JSX component by JavaScript. To do that, it first needs to know
some information about the template. Therefore, for every Drupal template
implemented as a JSX component by the theme, the theme needs to provide a
`TEMPLATE_NAME-template-info.json` file. For example, the Umami JSX theme
implements the `'views-view-grid'` Drupal template with a
[ViewsViewGrid.jsx](demo/themes/umami_jsx/app/src/components/views/drupal/ViewsViewGrid.jsx)
component, and therefore also provides a
[views-view-grid.template-info.json](demo/themes/umami_jsx/templates/views/views-view-grid.template-info.json)
file. The following is a simplified copy of that file (some props have been
removed to focus this documentation on the essential parts):
```
{
  "props": {
    "options": {
      "alignment": "string",
    },
    "items": [
      {
        "attributes": "object",
        "content": [
          {
            "attributes": "object",
            "content": "JSX.Element"
          }
        ]
      }
    ]
  }
}
```

The above says that `ViewsViewGrid.jsx` expects two props:
- An `options` object with an `alignment` string property.
- An `items` array, where each item in that array is an object containing:
  - An `attributes` object.
  - A `content` array, where each item in that array is an object containing:
    - An `attributes` object.
    - A `content` property whose value is a JSX Element (a JSX component or an
      HTML element).

Meanwhile, when Drupal renders a grid created with the Views module, for
a 2x2 grid PHP code similar to the following gets called:
```
jsx_render_template('views-view-grid', [
  'options' => ['alignment' => 'horizontal'],
  'items' => [
    [
      'attributes' => ['class' => ['custom-class-for-row-1']],
      'content' => [
        [
          'attributes' => ['class' => ['custom-class-for-cell-1a']],
          'content' => $content_of_cell_1a,
        ],
        [
          'attributes' => ['class' => ['custom-class-for-cell-1b']],
          'content' => $content_of_cell_1b,
        ],
      ],
    ],
    [
      'attributes' => ['class' => ['custom-class-for-row-2']],
      'content' => [
        [
          'attributes' => ['class' => ['custom-class-for-cell-2a']],
          'content' => $content_of_cell_2a,
        ],
        [
          'attributes' => ['class' => ['custom-class-for-cell-2b']],
          'content' => $content_of_cell_2b,
        ],
      ],
    ],
  ],
]);
```

Given the earlier props declarations in `views-view-grid.template-info.json`,
the `jsx_render_template()` function for the above PHP code return the
following:
```
<drupal-views-view-grid
  options='{"alignment": "horizontal"}'
  items='[
    {
      "attributes": {"class": ["custom-class-for-row-1"]},
      "content": [
        {"attributes": {"class": ["custom-class-for-cell-1a"]}},
        {"attributes": {"class": ["custom-class-for-cell-1b"]}}
      ]
    },
    {
      "attributes": {"class": ["custom-class-for-row-2"]},
      "content": [
        {"attributes": {"class": ["custom-class-for-cell-2a"]}},
        {"attributes": {"class": ["custom-class-for-cell-2b"]}}
      ]
    }
  ]'
>
  <drupal-html-fragment slot="items.0.content.0.content">...</drupal-html-fragment>
  <drupal-html-fragment slot="items.0.content.1.content">...</drupal-html-fragment>
  <drupal-html-fragment slot="items.1.content.0.content">...</drupal-html-fragment>
  <drupal-html-fragment slot="items.1.content.1.content">...</drupal-html-fragment>
</drupal-views-view-grid>
```

The `...` items in the above are the HTML output of rendering
`$content_of_cell_1a`, `$content_of_cell_1b`, `$content_of_cell_2a`, and
`$content_of_cell_2b`, respectively.

In other words, `jsx_render_template()` outputs a custom HTML element, named
the same as the template name (with `drupal-` prepended), where:
- The props that are not declared as having a type of `JSX.Element` are encoded
as JSON strings that are passed as attribute values on the custom element.
- The props that are declared as having a type of `JSX.Element` are rendered
into slots of the custom element, where the slot name is the JSON path of that
prop.

JavaScript code can now process this markup however it wants to, such as by
converting these custom elements into JSX components.

### The JavaScript side ([demo/themes/umami_jsx/app/src/index-*.js](demo/themes/umami_jsx/app/src/index-react.js))

TODO: Add docs about this.
